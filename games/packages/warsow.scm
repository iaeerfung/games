;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages warsow)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph))

;; TODO: Some assets are under a restrictive license, so the game won't probably make it upstream.
;; That said, the engine could.  A patch has been sent.  When merged, replace
;; the bundled engine with upstream's.
(define-public warsow
  (let ((arch (match (or (%current-target-system)
                         (%current-system))
                ("x86_64-linux" "x86_64")
                ("i686-linux" "i386")
                (_ ""))))
    (package
      (name "warsow")
      (version "2.1.2")
      (source
       (origin
         (method url-fetch)
         (uri (string-append "http://warsow.net/warsow-"
                             version
                             ".tar.gz"))
         (sha256
          (base32
           "07y2airw5qg3s1bf1c63a6snjj22riz0mqhk62jmfm9nrarhavrc"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:patchelf-plan
         `((,,(string-append "warsow." arch)
            ("sdl2"))
           (,,(string-append "libs/libangelwrap_" arch ".so")
            ("gcc"))
           (,,(string-append "libs/libui_" arch ".so")
            ("gcc"))
           (,,(string-append "libs/libsnd_qf_" arch ".so")
            ("sdl2"))
           (,,(string-append "libs/libref_gl_" arch ".so")
            ("sdl2")))
         #:install-plan
         `((,,(string-append "warsow." arch) "share/warsow/warsow")
           (,,(string-append "wsw_server." arch) "share/warsow/wsw_server")
           (,,(string-append "wswtv_server." arch) "share/warsow/wswtv_server")
           ("basewsw" "share/warsow/basewsw")
           ("docs" "share/warsow/docs")
           ("libs" "share/warsow/libs" #:include (,,(string-append "_" arch ".so"))))
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'fix-permissions
             (lambda _
               (for-each (lambda (dir)
                           (for-each (lambda (file)
                                       (chmod file #o644))
                                     (find-files dir ".*")))
                         '("basewsw" "libs" "docs"))
               #t))
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/warsow"))
                      (real (string-append output "/share/warsow/warsow")))
                 (make-wrapper wrapper real
                               `("LD_LIBRARY_PATH" ":" prefix
                                 ,(map (lambda (lib) (string-append (assoc-ref inputs lib) "/lib"))
                                       '("freetype" "libtheora"))))
                 (make-desktop-entry-file (string-append output "/share/applications/warsow.desktop")
                                          #:name "Warsow"
                                          #:exec wrapper
                                          ;; #:icon icon ; TODO: No icon?
                                          #:comment "Fast-paced first-person shooter set in a futuristic cartoonish world"
                                          #:categories '("Application" "Game")))
               #t)))))
      (inputs
       `(("gcc" ,gcc "lib")
         ("sdl2" ,sdl2)
         ;; Loaded at runtime:
         ("libtheora" ,libtheora)
         ("freetype" ,freetype)))
      (home-page "https://warsow.net/")
      (synopsis "Fast-paced first-person shooter set in a futuristic cartoonish world")
      (description "Speed and movement is what Warsow is all about.

Like a true cyberathlete you jump, dash, dodge, and walljump your way through
the game.  Grab power-ups before your enemy does, plant a bomb before anyone
sees you, and steal the enemy’s flag before they know what is going on!

The goal is to offer a fast and fun competitive first-person shooter without
hard graphical violence - Warsow has no blood or guts flying around.  Red
circles instead of blood indicate hits and colored triangles replace guts as
gib effects.

Great emphasis has been put on extreme customazibility and e-sports
features.")
      ;; The engine is under GPL2.
      ;; Some assets are under Creative Commons Attribution-NoDerivatives 4.0
      ;; International License.
      (license (list license:gpl2 (nonfree "file://share/warsow/docs/license.txt"))))))

(define-public warsow-2.5
  (let ((arch (match (or (%current-target-system)
                         (%current-system))
                ("x86_64-linux" "x86_64")
                (_ ""))))
    (package
      (inherit warsow)
      (version "2.5-beta")
      (source
       (origin
         (method url-fetch)
         (uri (string-append "http://warsow.net/warsow-"
                             (string-replace-substring version "-" "")
                             ".zip"))
         (sha256
          (base32
           "1dc1g9zapbhf3hrnbm67nxrxpzwr84lsjn3ii6lhyy0iy8m7a0p9"))))
      (supported-systems '("x86_64-linux"))
      (native-inputs
       `(("unzip" ,unzip)))
      (inputs
       `(("libuuid" ,util-linux)
         ,@(package-inputs warsow)))
      (arguments
       (substitute-keyword-arguments (package-arguments warsow)
         ((#:patchelf-plan _)
          ``((,,(string-append "warsow." arch)
              ("gcc" "libtheora" "libuuid" "sdl2"))
             (,,(string-append "libui_" arch ".so")
              ("gcc"))))
         ((#:install-plan _)
          ``((,,(string-append "warsow." arch) "share/warsow/warsow")
             (,,(string-append "wsw_server." arch) "share/warsow/wsw_server")
             ("basewsw" "share/warsow/basewsw")
             ("docs" "share/warsow/docs")
             (,,(string-append "libui_" arch ".so") "share/warsow/")))
         ((#:phases phases)
          `(modify-phases ,phases
             (replace 'create-wrapper
               (lambda* (#:key inputs outputs #:allow-other-keys)
                 (let* ((output (assoc-ref outputs "out"))
                        (wrapper (string-append output "/bin/warsow"))
                        (real (string-append output "/share/warsow/warsow")))
                   (make-wrapper wrapper real
                                 `("LD_LIBRARY_PATH" ":" prefix
                                   ,(map (lambda (lib) (string-append (assoc-ref inputs lib) "/lib"))
                                         '("freetype"))))
                   (make-desktop-entry-file (string-append output "/share/applications/warsow.desktop")
                                            #:name "Warsow"
                                            #:exec wrapper
                                            ;; #:icon icon ; TODO: No icon?
                                            #:comment "Fast-paced first-person shooter set in a futuristic cartoonish world"
                                            #:categories '("Application" "Game")))
                 #t)))))))))
