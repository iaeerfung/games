;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Alex Griffin <a@ajgrf.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages infinifactory)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (games build-system mojo)
  #:use-module (games gog-download)
  #:use-module (ice-9 match)
  #:use-module (nonguix licenses))

(define-public gog-infinifactory
  (let* ((arch (match (or (%current-target-system)
                          (%current-system))
                 ("x86_64-linux" "x86_64")
                 ("i686-linux" "x86")
                 (_ "")))
         (binary (string-append "infinifactory." arch))
         (plugins (string-append "infinifactory_Data/Plugins/" arch)))
    (package
      (name "gog-infinifactory")
      (version "2.0.0.3")
      (source
       (origin
         (method gog-fetch)
         (uri "gogdownloader://infinifactory/en3installer1")
         (file-name (string-append "gog_infinifactory_" version ".sh"))
         (sha256
          (base32
           "0cam9mgqdl0jr8q8ln7h893lpxyh4gl6bkf9s4irg838l3w30176"))))
      (supported-systems '("i686-linux" "x86_64-linux"))
      (build-system mojo-build-system)
      (arguments
       `(#:patchelf-plan
         `((,,binary
            ("libc" "eudev" "gcc:lib" "libstdc++" "libx11" "libxcursor"
             "libxrandr" "mesa" "pulseaudio" "zlib"))
           (,,(string-append plugins "/ScreenSelector.so")
            ("libc" "gcc:lib" "gdk-pixbuf" "glib" "gtk+" "libstdc++"))
           (,,(string-append plugins "/libfmod.so")
            ("libc" "libstdc++"))
           (,,(string-append plugins "/libfmodstudio.so")
            ("libc" "libstdc++")))))
      (inputs
       `(("eudev" ,eudev)
         ("gcc:lib" ,gcc "lib")
         ("gdk-pixbuf" ,gdk-pixbuf)
         ("glib" ,glib)
         ("gtk+" ,gtk+-2)
         ("libstdc++" ,(make-libstdc++ gcc))
         ("libx11" ,libx11)
         ("libxcursor" ,libxcursor)
         ("libxrandr" ,libxrandr)
         ("mesa" ,mesa)
         ("pulseaudio" ,pulseaudio)
         ("zlib" ,zlib)))
      (home-page "http://www.zachtronics.com/infinifactory/")
      (synopsis "Sandbox puzzle game about building factories")
      (description
       "Build factories that assemble products for your alien overlords, and
try not to die in the process.

@itemize
@item
LIKE SPACECHEM@dots{} IN 3D! - Design and run factories in a first-person,
fully 3D environment.
@item
HISTOGRAMS ARE BACK! - Optimize your solutions, and then optimize them more
when you see how much better your friends did.
@item
VISIT EXOTIC ALIEN LOCALES! - Explore a story-driven campaign with 50+ puzzles,
audio logs, and more.
@item
BLOCKS THAT MOVE! - Go beyond the campaign and push the limits of
Infinifactory’s next-generation block engine in the sandbox.
@end itemize")
      (license (undistributable
                (string-append "file://data/noarch/docs/"
                               "End User License Agreement.txt"))))))
